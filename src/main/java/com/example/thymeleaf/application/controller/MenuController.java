package com.example.thymeleaf.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

@Controller
@RequestMapping("menu")
public class MenuController {

    @Autowired
    SpringTemplateEngine templateEngine;

    @Autowired
    ThymeleafViewResolver viewResolver;

    @GetMapping
    public ModelAndView getMenu(ModelAndView _modelAndView) {
        _modelAndView.addObject("myVal", "Test Custom Text9999zzxz");
        _modelAndView.setViewName("menu");
        return _modelAndView;
    }

}
