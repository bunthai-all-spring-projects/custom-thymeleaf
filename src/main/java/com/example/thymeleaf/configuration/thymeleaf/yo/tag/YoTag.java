package com.example.thymeleaf.configuration.thymeleaf.yo.tag;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class YoTag extends AbstractElementTagProcessor {

    public YoTag(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, "iloveyo", true, null, false, 100);
    }

    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {
        structureHandler.setBody("Hello Yo!", false);
    }
}
