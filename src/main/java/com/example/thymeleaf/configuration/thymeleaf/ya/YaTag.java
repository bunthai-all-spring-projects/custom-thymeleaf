package com.example.thymeleaf.configuration.thymeleaf.ya;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class YaTag extends AbstractElementTagProcessor {

    public YaTag(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, "iloveya", true, null, false, 100);
    }

    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {
        structureHandler.setBody("Hello Ya!", false);
    }
}
