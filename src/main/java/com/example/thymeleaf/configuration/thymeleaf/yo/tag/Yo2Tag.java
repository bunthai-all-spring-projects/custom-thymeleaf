package com.example.thymeleaf.configuration.thymeleaf.yo.tag;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;
import org.unbescape.html.HtmlEscape;

public class Yo2Tag extends AbstractElementTagProcessor {

    public Yo2Tag(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, "iloveyo2", true, null, false, 100);
    }

    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {
        /*
         * Create the DOM structure that will be substituting our custom tag.
         * The headline will be shown inside a '<div>' tag, and so this must
         * be created first and then a Text node must be added to it.
         */
        final IModelFactory modelFactory = context.getModelFactory();

        final IModel model = modelFactory.createModel();

        model.add(modelFactory.createOpenElementTag("div"));
        model.add(modelFactory.createText(HtmlEscape.escapeHtml5("I love yo2")));
        model.add(modelFactory.createCloseElementTag("div"));

        // using parse
        model.addModel(modelFactory.parse(context.getTemplateData(), "<div style=\"color:darkgreen\"> Test Parse</div>"));

        /*
         * Instruct the engine to replace this entire element with the specified model.
         */
        structureHandler.replaceWith(model, false);
    }
}
