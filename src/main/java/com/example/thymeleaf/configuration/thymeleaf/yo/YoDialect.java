package com.example.thymeleaf.configuration.thymeleaf.yo;

import com.example.thymeleaf.configuration.thymeleaf.yo.attribute.YoAttribute;
import com.example.thymeleaf.configuration.thymeleaf.yo.attribute.YoTextAttribute;
import com.example.thymeleaf.configuration.thymeleaf.yo.tag.Yo2Tag;
import com.example.thymeleaf.configuration.thymeleaf.yo.tag.YoTag;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import java.util.HashSet;
import java.util.Set;

public class YoDialect extends AbstractProcessorDialect {

    private static final String DIALECT_NAME = "Yo Dialect";


    public YoDialect() {
        // We will set this dialect the same "dialect processor" precedence as
        // the Standard Dialect, so that processor executions can interleave.
        super(DIALECT_NAME, "yo", StandardDialect.PROCESSOR_PRECEDENCE);
    }


    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new YoAttribute(dialectPrefix));
        processors.add(new YoTag(dialectPrefix));
        processors.add(new Yo2Tag(dialectPrefix));
        processors.add(new YoTextAttribute(dialectPrefix));
        return processors;
    }
}
