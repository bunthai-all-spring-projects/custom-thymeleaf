package com.example.thymeleaf.configuration.thymeleaf.ya;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import java.util.HashSet;
import java.util.Set;

public class YaDialect extends AbstractProcessorDialect {

    private static final String DIALECT_NAME = "Ya Dialect";


    public YaDialect() {
        // We will set this dialect the same "dialect processor" precedence as
        // the Standard Dialect, so that processor executions can interleave.
        super(DIALECT_NAME, "ya", StandardDialect.PROCESSOR_PRECEDENCE);
    }


    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new YaTag(dialectPrefix));
        return processors;
    }
}
