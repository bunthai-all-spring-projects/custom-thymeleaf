package com.example.thymeleaf.configuration.thymeleaf.yo.attribute;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class YoAttribute extends AbstractAttributeTagProcessor {


    public YoAttribute(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, null, false, "bluetext", true, 100, false);
    }

    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, AttributeName attributeName, String attributeValue, IElementTagStructureHandler structureHandler) {
        structureHandler.setAttribute("style", "color:#1b248b");
    }
}
